#!/bin/sh

mkdir -p /.theia

if [ ! -f /.theia/recentworkspace.json ] ; then
    if [ -f /mnt/default/recentworkspace.json.default ]; then
        cp /mnt/default/recentworkspace.json.default /.theia/recentworkspace.json
    else
        echo "Can't find /mnt/default/recentworkspace.json.default"
    fi
fi

if [ ! -f /.theia/settings.json ] ; then
    if [ -f /mnt/default/settings.json.default ]; then
        cp /mnt/default/settings.json.default /.theia/settings.json
    else
        echo "Can't find /mnt/default/settings.json.default"
    fi
fi

cd /tmp/theia-install
cp -rp plugins /.theia

yarn theia start --hostname $(hostname)