# ##### Dockerfile for Watson Machine Learning Community Edition 1.7.0 #####
# sudo docker build -t registry.gitlab.com/pslc/docker/wmlce:1.7.0 .
# sudo docker push registry.gitlab.com/pslc/docker/wmlce:1.7.0

FROM node:10.20.1-alpine

RUN apk add --update sudo bash man git python python3 py3-flake8 make gcc g++ libc-dev

COPY theia-package.json /tmp/theia-install/package.json

RUN adduser -S -u 3000 -G root -h /myDev -s /bin/bash theia && \
	chmod 2770 /myDev && \
	chown -R 3000:0 /tmp/theia-install && \
	mkdir -p /.theia && chmod 2770 /.theia && \
	mkdir -p /.cache && chmod 2770 /.cache && \
	mkdir -p /.yarn &&	chmod 2770 /.yarn

USER 3000

# Install Theia
RUN cd /tmp/theia-install && \
#	npm install -g yarn && \
	yarn && \
	yarn theia build

COPY settings.json /mnt/default/settings.json.default
COPY recentworkspace.json /mnt/default/recentworkspace.json.default
# Install NGINX Proxy
# RUN sudo apt-get install -y \
#    nginx \
#    apache2-utils

#COPY start.sh /start.sh

COPY tini-ppc64le /tini
COPY start.sh /start.sh

WORKDIR /tmp/theia-install

ENV THEIA_DEFAULT_PLUGINS local-dir:/tmp/theia-install/plugins

EXPOSE 3000

CMD [ "/tini", "/start.sh" ]
